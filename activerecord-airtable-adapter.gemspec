# frozen_string_literal: true

require_relative "lib/activerecord/airtable/adapter/version"

Gem::Specification.new do |spec|
  spec.name = "activerecord-airtable-adapter"
  spec.version = Activerecord::Airtable::Adapter::VERSION
  spec.authors = ["Harold Armijo"]
  spec.email = ["harold.armijo.leon@byseven.co"]

  spec.summary = "This gem adds an adapter rails for the AIRTABLE tool."
  spec.description = "This gem adds an adapter rails for the AIRTABLE tool."
  spec.homepage = "https://gitlab.com/by-seven/open-tools/activerecord-airtable-adapter"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["allowed_push_host"] = "TODO: Set to your gem server 'https://example.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/by-seven/open-tools/activerecord-airtable-adapter"
  spec.metadata["changelog_uri"] = "https://gitlab.com/by-seven/open-tools/activerecord-airtable-adapter"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (File.expand_path(f) == __FILE__) || f.start_with?(*%w[bin/ test/ spec/ features/ .git .circleci appveyor])
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  spec.add_dependency "activerecord", "~> 7.0.5"
  spec.add_dependency "httparty", "~> 0.21.0"

  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html
end
