# frozen_string_literal: true

module Airtable
  class Database < Resource

    def exists?
      result = self.class.get(tables_url).parsed_response

      if result["error"] == "NOT_FOUND"
        return false
      end

      !check_and_raise_error(result)
    end

    def create!
      result = self.class.post(
        databases_url,
        body: {
          name: db_id,
          workspaceId: workspace_id,
          tables: [
            {
              name: "tmp",
              description: "A temporary table",
              fields: [
                {
                  name: "Name",
                  description: "Name of the apartment",
                  type: "singleLineText"
                },
              ],
            }
          ]
        }.to_json).parsed_response

      check_and_raise_error(result)
    end

    def tables
      result = self.class.get(tables_url).parsed_response

      check_and_raise_error(result)

      result["tables"].map {|t| t["name"]}
    end

    private

    def databases_url
      "/meta/bases"
    end
  end
end
