module Airtable

  # Contains records and the offset after a record query
  class RecordSet < SimpleDelegator

    attr_reader :records, :offset, :table

    # results = { "records" => [{ ... }], "offset" => "abc5643" }
    # response from records api
    def initialize(table, klass, results)
      @table = table
      col_without_ref = table.column_def_without_ref.map {|r| r["name"]}
      # Parse records
      @records = if results && results["records"]
                   results["records"].map do |r|
                     Record.new(
                       table,
                       klass,
                       r["fields"].merge("id" => r["id"]),
                       r["fields"].filter {|k, v| col_without_ref.include?(k)}.merge("id" => r["id"])
                     )
                   end
                 else
                   []
                 end
      # Store offset
      @offset = results["offset"] if results
      # Assign delegation object
      __setobj__(@records)
    end

    def records=(rec)
      __setobj__(rec)
    end

    def find_or_initialize_by(**attrs)
      if record = table.find_by(**attrs)
        record
      else
        record = table.schema_def
        record.override_attributes(attrs)
        record
      end
    end

  end # Record

end # Airtable