module Airtable
  # Base class for authorized resources sending network requests
  class Resource
    include HTTParty
    base_uri 'https://api.airtable.com/v0/'
    # debug_output $stdout

    attr_reader :api_key, :workspace_id, :db_id, :worksheet_name, :klass

    def initialize(api_key, workspace_id = nil, db_id = nil, worksheet_name = nil, klass = nil)
      @api_key = api_key
      @workspace_id = workspace_id
      @db_id = db_id
      @worksheet_name = worksheet_name
      @klass = klass
      self.class.headers(
        {
          'Authorization' => "Bearer #{@api_key}",
          "Content-type" => "application/json"
        },
      )
    end

    protected

    def check_and_raise_error(response)
      response['error'] ? raise(Error.new(response['error'])) : false
    end

    def tables_url
      "/meta/bases/#{db_id}/tables"
    end

  end # AuthorizedResource
end # Airtable