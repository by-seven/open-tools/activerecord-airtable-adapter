# Allows access to data on airtable
#
# Fetch all records from table:
#
# client = Airtable::Client.new("keyPtVG4L4sVudsCx5W")
# client.table("appXXV84QuCy2BPgLk", "Sheet Name").all
#

module Airtable
  class Client
    attr_reader :api_key, :workspace_id, :db_id

    def initialize(api_key, workspace_id, db_id)
      @api_key = api_key
      @workspace_id = workspace_id
      @db_id = db_id
    end

    # table("appXXV84QuCy2BPgLk", "Sheet Name")
    def table(worksheet_name, klass = nil)
      Table.new(api_key, workspace_id, db_id, worksheet_name, klass)
    end

    def database
      Database.new(api_key, workspace_id, db_id)
    end
  end # Client
end # Airtable