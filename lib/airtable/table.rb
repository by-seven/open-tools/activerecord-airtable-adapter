module Airtable

  class Table < Resource
    # Maximum results per request
    LIMIT_MAX = 100

    # Fetch all records iterating through offsets until retrieving the entire collection
    # all(:sort => ["Name", :desc])
    def all(options={})
      offset = nil
      results = []
      begin
        options.merge!(:limit => LIMIT_MAX, :offset => offset)
        response = records(options)
        results += response.records
        offset = response.offset
      end until offset.nil? || offset.empty? || results.empty?
      rec_set = RecordSet.new(self, klass, nil)
      rec_set.records = results
      rec_set
    end

    # Fetch records from the sheet given the list options
    # Options: limit = 100, offset = "as345g", sort = ["Name", "asc"]
    # records(:sort => ["Name", :desc], :limit => 50, :offset => "as345g")
    def records(options={})
      options["sortField"], options["sortDirection"] = options.delete(:sort) if options[:sort]
      results = self.class.get(worksheet_url, query: options).parsed_response
      check_and_raise_error(results)
      RecordSet.new(self, klass, results)
    end

    # Query for records using a string formula
    # Options: limit = 100, offset = "as345g", sort = ["Name", "asc"],
    #          fields = [Name, Email], formula = "Count > 5", view = "Main View"
    #
    # select(limit: 10, sort: ["Name", "asc"], formula: "Order < 2")
    def select(options={})
      options['sortField'], options['sortDirection'] = options.delete(:sort) if options[:sort]
      options['maxRecords'] = options.delete(:limit) if options[:limit]

      if options[:formula]
        raise_bad_formula_error unless options[:formula].is_a? String
        options['filterByFormula'] = options.delete(:formula)
      end

      results = self.class.get(worksheet_url, query: options).parsed_response
      check_and_raise_error(results)
      RecordSet.new(self, klass, results)
    end

    def raise_bad_formula_error
      raise ArgumentError.new("The value for filter should be a String.")
    end

    # Returns record based given row id
    def find(id)
      result = self.class.get(worksheet_url + "/" + id).parsed_response
      check_and_raise_error(result)
      Record.new(
        self,
        klass,
        result_attributes(result),
        result_attributes_without_ref(result)
      ) if result.present? && result["id"]
    end

    def find_by(**attrs)
      where(1, **attrs).first
    end

    def where(limit = LIMIT_MAX, **attrs)
      req = []
      attrs.each do |k, v|
        req << "#{k} = \"#{v}\""
      end
      select(formula: "AND(#{req.join(', ')})", limit: limit)
    end

    # Creates a record by posting to airtable
    def create(record)
      record = if record.is_a?(Airtable::Record)
                 record
               else
                 col_without_ref = column_def_without_ref.map {|r| r["name"]}
                 Airtable::Record.new(
                   self,
                   klass,
                   record,
                   record.filter {|k, v| col_without_ref.include?(k)}
                 )
               end
      record.created_at = Time.now if schema_def.respond_to?(:created_at)
      record.updated_at = Time.now if schema_def.respond_to?(:updated_at)

      result = self.class.post(worksheet_url,
        :body => { "fields" => record.fields }.to_json,
        :headers => { "Content-type" => "application/json" }).parsed_response

      check_and_raise_error(result)
      record.override_attributes!(result_attributes(result))
      record
    end

    # Replaces record in airtable based on id
    def update(record)
      if record.new_record?
        create(record)
      else
        record.updated_at = Time.now if schema_def.respond_to?(:updated_at)
        result = self.class.put(worksheet_url + "/" + record.id,
                                :body => { "fields" => record.fields_for_update }.to_json,
                                :headers => { "Content-type" => "application/json" }).parsed_response

        check_and_raise_error(result)

        record.override_attributes!(result_attributes(result))
        record
      end
    end

    def update_record_fields(record_id, fields_for_update)
      result = self.class.patch(worksheet_url + "/" + record_id,
        :body => { "fields" => fields_for_update }.to_json,
        :headers => { "Content-type" => "application/json" }).parsed_response

      check_and_raise_error(result)

      Record.new(self, klass, result_attributes(result), result_attributes_without_ref(result))
    end

    # Deletes record in table based on id
    def destroy(id)
      self.class.delete(worksheet_url + "/" + id).parsed_response
    end

    def exists?
      !!(get_table(worksheet_name))
    end

    def create_table(table_definition)
      result = self.class.post(tables_url, {
        body: {
          name: worksheet_name,
          fields: table_definition
        }.to_json
      }).parsed_response

      check_and_raise_error(result)

      result
    end

    def column_def
      get_table(worksheet_name)["fields"]
    end

    def column_def_without_ref
      get_table(worksheet_name)["fields"].filter { |item| item["type"] != "multipleRecordLinks" }
    end

    def get_id(table_name)
      get_table(table_name)["id"]
    end

    def schema_def
      result = Hash[column_def.map {|c| [c["name"], nil]}]
      result["id"] = nil
      result_without_ref = Hash[column_def_without_ref.map {|c| [c["name"], nil]}]
      result_without_ref["id"] = nil
      Record.new(self, klass, result, result_without_ref)
    end

    protected

    def get_table(table_name)
      result = self.class.get(tables_url).parsed_response

      check_and_raise_error(result)

      result["tables"].find {|t| t["name"] == table_name}
    end

    def result_attributes(res)
      res["fields"].merge("id" => res["id"]) if res.present? && res["id"]
    end

    def result_attributes_without_ref(res)
      col_without_ref = table.column_def_without_ref.map {|r| r["name"]}
      res["fields"].filter {|k, v| col_without_ref.include?(k)}.merge("id" => res["id"]) if res["id"]
    end

    def worksheet_url
      "/#{db_id}/#{url_encode(worksheet_name)}"
    end

    # From http://apidock.com/ruby/ERB/Util/url_encode
    def url_encode(s)
      s.to_s.dup.force_encoding("ASCII-8BIT").gsub(/[^a-zA-Z0-9_\-.]/) {
        sprintf("%%%02X", $&.unpack("C")[0])
      }
    end
  end # Table

end # Airtable
