# frozen_string_literal: true

module Activerecord
  module Airtable
    module Adapter
      VERSION = "0.1.0"
    end
  end
end
