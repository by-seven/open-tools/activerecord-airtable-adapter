# frozen_string_literal: true

require "active_record"
require "active_support/all"
require 'delegate'
require "httparty"

require "activerecord/airtable/adapter/version"
require 'active_record/connection_adapters/statement_pool'
require 'active_record/connection_adapters/airtable/schema_statements'
require 'active_record/connection_adapters/airtable/database_statements'
require 'active_record/connection_adapters/airtable/query'
require "active_record/connection_adapters/airtable_adapter"
require "active_record/tasks/database_tasks"
require "active_record/tasks/airtable_database_tasks"

require "airtable/resource"
require "airtable/client"
require "airtable/database"
require "airtable/record"
require "airtable/record_set"
require "airtable/table"
require "airtable/client"
require "airtable/error"

module Activerecord
  module Airtable
    module Adapter
      class Error < StandardError; end
      # Your code goes here...
    end
  end
end

ActiveRecord::Tasks::DatabaseTasks.class_eval do
  register_task(/airtable/, "ActiveRecord::Tasks::AirtableDatabaseTasks")
end

ActiveSupport.on_load :active_record do
  extend ActiveRecord::ConnectionAdapters::Airtable::Query
end
