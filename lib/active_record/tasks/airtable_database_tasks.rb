# frozen_string_literal: true

module ActiveRecord
  module Tasks
    class AirtableDatabaseTasks

      def self.using_database_configurations?
        true
      end

      def initialize(db_config)
        @db_config = db_config
        @configuration_hash = db_config.configuration_hash
      end

      def create
        connection.create_database
      end


      private
      attr_reader :db_config, :configuration_hash

      def connection
        ActiveRecord::Base.connection
      end

      def establish_connection(config = db_config)
        ActiveRecord::Base.establish_connection(config)
      end
    end
  end
end