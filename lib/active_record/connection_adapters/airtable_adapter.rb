

module ActiveRecord
  module ConnectionHandling
    def airtable_adapter_class
      ConnectionAdapters::AirtableAdapter
    end

    def airtable_connection(config)
      airtable_adapter_class.new(logger, config)
    end
  end

  module ConnectionAdapters
    class AirtableAdapter < AbstractAdapter
      attr_reader :client, :database_id
      ADAPTER_NAME = "Airtable"

      include Airtable::SchemaStatements
      include Airtable::DatabaseStatements

      NATIVE_DATABASE_TYPES = {
        primary_key:  "any",
        string:       { name: "singleLineText" },
        bigint:       { name: "multipleRecordLinks" },
        text:         { name: "multilineText" },
        integer:      { name: "number", options: {precision: 8} },
        float:        { name: "number", options: {precision: 8} },
        decimal:      { name: "number", options: {precision: 8} },
        datetime:     { name: "dateTime", options: {
          timeZone: "Europe/Paris",
          dateFormat: {
            format: "YYYY-MM-DD",
            name: "iso"
          },
          timeFormat: {
            format: "HH:mm",
            name: "24hour"
          }
        } },
        time:         { name: "createdTime" },
        date:         { name: "date", options: {
          dateFormat: {
            format: "YYYY-MM-DD",
            name: "iso"
          }
        } },
        binary:       { name: "multipleAttachments" },
        boolean:      { name: "checkbox", options: {color: "greenBright", icon: "check"} },
        json:         { name: "multilineText" },
      }

      class StatementPool < ::ActiveRecord::ConnectionAdapters::StatementPool # :nodoc:
      end

      def initialize(logger, config)

        @config = config
        @pat = @config[:pat]
        @database_id = @config[:database]
        @client = ::Airtable::Client.new(@config[:pat], @config[:workspace_id], @config[:database])

        super(@client, logger, config)
      end

      def database_exists?
        database = client.database

        database.exists?
      end

      def create_database
        raise DatabaseAlreadyExists if database_exists?

        database = client.database
        database.create!
      end


      def native_database_types # :nodoc:
        NATIVE_DATABASE_TYPES
      end

      def primary_keys(table_name) # :nodoc:
        []
      end

      private

      def build_statement_pool
        StatementPool.new(self.class.type_cast_config_to_integer(@config[:statement_limit]))
      end
    end
  end
end