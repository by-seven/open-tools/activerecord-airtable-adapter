# frozen_string_literal: true

module ActiveRecord
  module ConnectionAdapters
    module Airtable
      module Query

        def all
          table = connection.client.table(table_name, self)
          table.records
        end

        def find(id)
          table = connection.client.table(table_name, self)
          table.find(id.to_s)
        end

        def where(**attrs)
          # Assuming args is a hash with attribute names as keys and desired values
          table = connection.client.table(table_name, self)

          table.where(**attrs)
        end

        def create(**attrs)
          table = connection.client.table(table_name, self)

          table.create(attrs)
        end

        def create!(**attrs)
          self.create(**attrs)
        end

        def find_by(**args)
          # Assuming args is a hash with attribute names as keys and desired values
          table = connection.client.table(table_name, self)

          table.find_by(**args)
        end

        def order(*args)
          raise ArgumentError if args.count != 1

          table = connection.client.table(table_name, self)
          table.all(
            sort: [
              args[0].is_a?(Symbol) ? args[0] : args[0].keys[0],
              args[0].is_a?(Symbol) ? :asc : args[0].values[0]
            ]
          )
        end
      end
    end
  end
end