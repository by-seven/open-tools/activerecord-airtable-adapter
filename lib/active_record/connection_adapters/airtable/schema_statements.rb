# frozen_string_literal: true

module ActiveRecord
  module ConnectionAdapters
    module Airtable
      module SchemaStatements

        def table_exists?(name)
          client.table(name).exists?
        end

        # def execute(sql, name = nil)
        #   binding.pry
        # end
        def create_table(table_name, id: :primary_key, primary_key: nil, force: nil, **options)
          td = create_table_definition(table_name, **extract_table_options!(options))

          if id && !td.as
            # TODO: something? in aitable the primary key is the first field created on the table
            append_primary_key = true
          end
          yield td if block_given?

          if force
            raise NotImplementedError
          else
            schema_cache.clear_data_source_cache!(table_name.to_s)
          end
          # remap types
          airtable_table_def = td.columns.map do |c|
            if c.type == :string && c.options && c.options[:options] && c.options[:options][:choices]
              c.options =  c.options[:options]
              c.type = :singleSelect
            elsif c.type == :bigint && td.foreign_keys.any?{|f_key| f_key.options && f_key.options[:column] == c.name }
              c.type = :multipleRecordLinks
              reference_table_name = td.foreign_keys.find{|f_key| f_key.options[:column] == c.name }.to_table
              reference_table_id = client.table(table_name).get_id(reference_table_name)
              c.options = { linkedTableId: reference_table_id }
            else
              c.options = native_database_types[c.type][:options]

              remove_opt = native_database_types[c.type][:options].blank?
              c.type = native_database_types[c.type][:name]
            end

            # TODO: refactor to get options and to take into account primary col(in airtable is the first item of the array table)
            line = c.to_h.except(:sql_type)
            if remove_opt
              line.except(:options)
            else
              line
            end
          end
          client.table(table_name).create_table(airtable_table_def)
        end

        def column_definitions(table_name)
          fields = client.table(table_name).column_def
          fields << {"name" => "id", "type" => "singleLineText"}
          fields
        end

        def new_column_from_field(table_name, field)
          type_metadata = fetch_type_metadata(field["type"])

          Column.new(
            field["name"],
            nil,
            type_metadata,
            true,
            nil,
          )
        end

        def data_source_sql(name = nil, type: nil)
          scope = quoted_scope(name, type: type)
          scope[:type] ||= "'table','view'"

          sql = +"SELECT name FROM sqlite_master WHERE name <> 'sqlite_sequence'"
          sql << " AND name = #{scope[:name]}" if scope[:name]
          sql << " AND type IN (#{scope[:type]})"
          sql
        end

        def quoted_scope(name = nil, type: nil)
          type = \
            case type
            when "BASE TABLE"
              "'table'"
            when "VIEW"
              "'view'"
            end
          scope = {}
          scope[:name] = quote(name) if name
          scope[:type] = type if type
          scope
        end

        def data_source_exists?(name)
          table = client.table(name)

          table.exists?
        end

        def indexes(table_name)
          []
        end

        def tables
          db = client.database

          db.tables
        end
      end
    end
  end
end
