# frozen_string_literal: true

module ActiveRecord
  module ConnectionAdapters
    module Airtable
      module DatabaseStatements
        def exec_query(sql, name = nil, binds = [], prepare: false, async: false) # :nodoc:
          build_result(columns: [], rows: [])
        end
      end
    end
  end
end
